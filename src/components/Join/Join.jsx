import React, { useState, useEffect } from "react";
import "./Join.css";
import {
  Form,
  Navbar,
  Nav,
  NavDropdown,
  OverlayTrigger,
  Button,
  ButtonGroup,
  Tooltip,
  Tabs,
  Tab,
} from "react-bootstrap";
import { css } from "glamor";
import ScrollToBottom from "react-scroll-to-bottom";
import moment from "moment-timezone";
import _ from "lodash";
import LoginForm from "../../shared/LoginForm/LoginForm";
import RegisterForm from "../../shared/RegisterForm/RegisterForm";
import { useHistory } from "react-router-dom";
import Skeleton from "@material-ui/lab/Skeleton";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  skeletonBar: {
    width: "100%",
  },
});
const ROOT_CSS = css({
  height: "100%",
});

const Join = () => {
  const materialClasses = useStyles();
  let history = useHistory();
  const endpoint = 'http://34.85.53.207:5000'; // prod location,todo setup env variables
  // const endpoint = "http://127.0.0.1:5000"; // local location,todo , setup env variables
  const cov19Url =
    "https://coronavirus-19-api.herokuapp.com/countries/Hong Kong";
  const [options, setOptions] = useState();
  const [cov19Lists, setCov19Lists] = useState();
  const [news, setNews] = useState([]);
  const [loginForm, setLoginForm] = useState({
    username: "",
    password: "",
    room: "",
  });
  const [registerForm, setRegisterForm] = useState({
    username: "",
    password: "",
  });

  useEffect(() => {
    const { username, password, room } = loginForm;
    if (username.length > 0 && password.length > 0) {
      const requestOptions = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({ username: username, password: password }),
      };
      fetch(endpoint + "/login", requestOptions)
        .then((response) => response.json())
        .then(async (data) => {
          // const {status, data} = data; //  'data' has already been declared
          const { loginStatus, message, token } = data;
          if (loginStatus) {
            alert(message);
            await localStorage.setItem("token", token);
            history.push({
              pathname: "/chat",
              state: {
                username: username,
                room: room,
              },
            });
          } else {
            alert(message);
          }
        })
        .catch((e) => console.log(e));
    }
  }, [loginForm, history]);

  useEffect(() => {
    const { username, password } = registerForm;
    if (username.length > 0 && password.length > 0) {
      const requestOptions = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({ username: username, password: password }),
      };
      fetch(endpoint + "/createUser", requestOptions)
        .then((response) => response.json())
        .then((data) => {
          // const {status, data} = data; //  'data' has already been declared
          const { status } = data;
          if (status) {
            alert(data["data"]);
          } else {
            alert(data["data"]);
          }
        })
        .catch((e) => console.log(e));
    }
  }, [registerForm]);

  useEffect(() => {
    setNews([]);
    setOptions(null);
    setCov19Lists(null);
    if (!options) {
      fetch(endpoint + "/getRoom")
        .then((response) => response.json())
        .then((data) => setOptions(data));
    }
    if (!cov19Lists) {
      fetch(cov19Url)
        .then((response) => response.json())
        .then((data) => setCov19Lists(data));
    }

    if (_.isEmpty(news)) {
      fetch(endpoint + "/news")
        .then((response) => response.json())
        .then((data) => {
          if (data.status === "ok") {
            setNews((res) => [...res, ...data.articles]);
          }
        });
    }
  }, [null]);

  const renderTooltip1 = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      Hong Kong COV-19 Total Case
    </Tooltip>
  );

  const renderTooltip2 = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      Today Case
    </Tooltip>
  );

  return (
    <div>
      <Navbar
        bg="dark"
        variant="dark"
        style={{ position: "static", width: "100%" }}
      >
        <Form inline>
          <ButtonGroup size="sm">
            <OverlayTrigger
              placement="bottom"
              delay={{ show: 250, hide: 400 }}
              overlay={renderTooltip1}
            >
              <Button variant="outline-danger">
                {cov19Lists ? cov19Lists["cases"] : 0}
              </Button>
            </OverlayTrigger>
            <OverlayTrigger
              placement="bottom"
              delay={{ show: 250, hide: 400 }}
              overlay={renderTooltip2}
            >
              <Button variant="outline-warning">
                {cov19Lists ? cov19Lists["todayCases"] : 0}
              </Button>
            </OverlayTrigger>
          </ButtonGroup>
        </Form>
        <Nav className="nav navbar-nav ml-auto">
          <NavDropdown title="工具" id="collasible-nav-dropdown" alignRight>
            <NavDropdown.Item href="/">Excel2Json</NavDropdown.Item>
            <NavDropdown.Item href="/">Json2Excel</NavDropdown.Item>
          </NavDropdown>
          {/* <Nav.Link href="/">Excel2Json工具</Nav.Link> */}
        </Nav>
      </Navbar>
      <div className="joinOuterContainer">
        <div className="joinOuterInnerContainer">
          <ScrollToBottom className={ROOT_CSS}>
            <ul>
              {!_.isEmpty(news) ? (
                news.map((data, i) => (
                  <li key={i}>
                    <h3>
                      {data
                        ? moment
                            .tz(data.publishedAt, "Asia/Hong_Kong")
                            .format("ll")
                        : null}
                    </h3>
                    <p>{data.title}</p>
                  </li>
                ))
              ) : (
                <>
                  <li>
                    <Skeleton
                      className={materialClasses.skeletonBar}
                      animation="wave"
                    />
                  </li>
                  <li>
                    <Skeleton
                      className={materialClasses.skeletonBar}
                      animation="wave"
                    />
                  </li>
                  <li>
                    <Skeleton
                      className={materialClasses.skeletonBar}
                      animation="wave"
                    />
                  </li>
                  <li>
                    <Skeleton
                      className={materialClasses.skeletonBar}
                      animation="wave"
                    />
                  </li>
                  <li>
                    <Skeleton
                      className={materialClasses.skeletonBar}
                      animation="wave"
                    />
                  </li>
                  <li>
                    <Skeleton
                      className={materialClasses.skeletonBar}
                      animation="wave"
                    />
                  </li>
                  <li>
                    <Skeleton
                      className={materialClasses.skeletonBar}
                      animation="wave"
                    />
                  </li>
                </>
              )}
            </ul>
          </ScrollToBottom>
        </div>

        <div className="joinInnerContainer">
          <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
            <Tab eventKey="login" title="login" className="tabContainer">
              <LoginForm setLoginForm={setLoginForm} options={options} />
            </Tab>
            <Tab eventKey="register" title="register" className="tabContainer">
              <RegisterForm setRegisterForm={setRegisterForm} />
            </Tab>
          </Tabs>
        </div>
      </div>
    </div>
  );
};

// const mapStateToProps = (state) => (
//     console.log(state)
// );

// let mapDispatchToProps = dispatch => ({
//     increase: () => dispatch({type: 'INCREASE'}),
//     decrease: () => dispatch({type: 'DECREASE'})
// });

// export default connect(mapStateToProps)(Join);
export default Join;
