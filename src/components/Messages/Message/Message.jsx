import React from 'react';

import './Message.css';
import moment from 'moment-timezone';
import ReactEmoji from 'react-emoji';
import _ from 'lodash';


const Message = ({ message: { message, username, create_at }, name, userColors }) => {
  const getRandomColor = (user) => {
    let data =  _.find(userColors, function(o) { return o.name === user; });
    let defaultColor = "black";
    try {
      defaultColor = data.color;
    }catch(e) {
      defaultColor = "black";
    }
    return defaultColor; 
  }
  let isSentByCurrentUser = false;
  const trimmedName = name.trim().toLowerCase();

  if(username === trimmedName) {
    isSentByCurrentUser = true;
  }

  return (
    isSentByCurrentUser
      ? (
          <div className="message sent">
              <h5 style={{display: "block"}}>{username}</h5>
              {ReactEmoji.emojify(message)}
              <span className="metadata">
                  <span className="time">{create_at ? moment.unix(create_at).tz("Asia/Hong_Kong").format('lll'): null}</span>
              </span>
          </div>
        )
        : (
          <div className="message received">
            <h5 style={{display: "block",color: getRandomColor(username)}}>{username}</h5>
            {ReactEmoji.emojify(message)}
            <span className="metadata"><span className="time">{create_at ?  moment.unix(create_at).tz("Asia/Hong_Kong").format('lll'): null}</span></span>
          </div>
        )
  );
}

export default Message;