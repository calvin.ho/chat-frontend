import React from 'react';

import ScrollToBottom from 'react-scroll-to-bottom';

import Message from './Message/Message';

import './Messages.css';
import { css } from 'glamor';

const ROOT_CSS = css({
  height: '100%',
  padding: 5% 0,
  overflow: 'auto',
  flex: 'auto'
});



const Messages = ({ messages, name, userColors}) => (
  <div style={{height: '100%'}}>
  <ScrollToBottom className={ROOT_CSS}> 
    {
      messages.map((message, i) => <div key={i}>
        <Message message={message} name={name} userColors={userColors}/></div>)
    }
  </ScrollToBottom>
  </div>
);

export default Messages;