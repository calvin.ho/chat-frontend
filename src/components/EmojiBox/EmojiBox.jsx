import React from 'react';
import './EmojiBox.css';
import { Picker } from 'emoji-mart';


const EmojiBox = ({ setEmoji, sendEmoji}) => {

    const addEmoji = e => {
      setEmoji(e.native)
      sendEmoji(e);
    };
  return (
    <span>
      <Picker className="picker"  useButton={true} onClick={(e) => addEmoji(e)}/>
    </span>
  );
}
  
export default EmojiBox;