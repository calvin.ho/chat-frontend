import React, { useState, useRef } from 'react';
import './Input.css';
import { Button, Overlay } from 'react-bootstrap';
import ReactEmoji from 'react-emoji';
import EmojiBox from '../EmojiBox/EmojiBox';

const Input = ({ setMessage, sendMessage, message }) => {
  const [emoji, setEmoji] = useState();
  const [show, setShow] = useState(false);
  const target = useRef(null);
  const sendEmoji = () => {
    // event.preventDefault();
    if(emoji) {
      setMessage(message + emoji);
    }
  }
  return(
    <form className="form">
      <Overlay target={target.current} show={show} placement="top">
        {({
          placement,
          scheduleUpdate,
          arrowProps,
          outOfBoundaries,
          show: _show,
          ...props
        }) => (
          <div
            {...props}
            style={{
              padding: '9px 2px',
              color: 'white',
              borderRadius: 3,
              ...props.style,
            }}
          >
           <EmojiBox setEmoji={setEmoji} sendEmoji={sendEmoji}></EmojiBox>
          </div>
        )}
      </Overlay>
      <input
        className="input"
        type="text"
        placeholder="Type a message..."
        value={message}
        onChange={event => setMessage(event.target.value)}
        onKeyPress={event => event.key === 'Enter' ? sendMessage(event) : null}
      />
      <Button type="button" className="emojiButton" variant="light"ref={target} onClick={() => setShow(!show)}>{ReactEmoji.emojify(':wink:')}</Button>
      {/* <ButtonGroup size="sm" style={{'padding': '0px', 'flex': '50%','display': 'flex'}}>
        <Button type="button" variant="light"ref={target} onClick={() => setShow(!show)}>{ReactEmoji.emojify(':wink:')}</Button>
        <Button onClick={e => sendMessage(e)}>Send</Button>
      </ButtonGroup> */}
    </form>
  );
}

export default Input;