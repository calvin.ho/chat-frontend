import React, { useState, useEffect } from 'react';
// import quertString from 'query-string';
import io from 'socket.io-client';
import './Chat.css';
import InfoBar from '../InfoBar/InforBar';
import Input from '../Input/Input';
import Messages from '../Messages/Messages';
import TextContainer from '../TextContainer/TextContainer';
import { css } from "@emotion/core";
import { ClimbingBoxLoader } from "react-spinners";
import _ from 'lodash';
import { useLocation } from "react-router-dom";
import { useHistory } from "react-router-dom";

let socket;
const Chat = (props) => {
    let history = useHistory();
    const endpoint = '34.85.53.207:5000';
    // const endpoint = 'localhost:5000';
    const [name, setName] = useState('');
    const [room, setRoom] = useState('');
    const [message, setMessage] = useState('');
    const [messages, setMessages] = useState([]);
    const [users, setUsers] = useState('');
    const [err, setErr] = useState(false);
    const [userColors, setUserColors] = useState([]);
    const override = css`
        display: block;
        margin: 0 auto;
        border-color: red;
    `;
  
    const location = useLocation();

    useEffect(() => {
        // const {name, room} = quertString.parse(location.search);
        try {
            const {token, room, name} = {
                token: localStorage.getItem('token'),
                name: location.state.username,
                room: location.state.room
            };

            setRoom(room);
            setName(name);

            socket = io(endpoint, { query: "token="+token });
            socket.emit('join', {name, room}, (data, err) => {
                if(err) {
                    alert(err);
                }else {
                    setErr(true)
                }
            });

            return () => {
                socket.emit('disconnect');
                socket.off();
            }
        }catch(e) {
            history.push("/");
        }
    },[history, location]);


    useEffect(() => {
        try {
            socket.on('message', message => {
                setMessages(messages => [ ...messages, message ]);
            });
            
            socket.on("roomData", ({ users }) => {
                setUsers(users);
            });
            socket.on("roomData", ({ users }) => {
                setUsers(users);
            });

            socket.on("errMsg", (res) => {
                if(res) {
                    setErr(true)
                }else {
                    setErr(false)
                }
            });

            socket.on('historys', (historys) => {
                setMessages(messages => [ ...messages, ...historys ]);
            });
        }catch(e) {
            history.push("/");
        }
    }, [history]);
    
    useEffect(()=> {
        const colors = ['red', 'orange', 'green', 'blue', 'thistle'];
        if(_.size(userColors) <= 0) {
            _.map(_.uniqBy(messages, 'username'), 'username').forEach(name => {
                setUserColors(data => [ ...data, {
                    'name': name,
                    'color': colors[Math.floor(Math.random() * colors.length)]
                }]);
            });
        }
    })

    const sendMessage = (event) => {
        event.preventDefault();
        if(message) {
            socket.emit('sendMessage' , message, () => setMessage(''));
        }
    }

    const reConnect = () => {
        socket.io.reconnect();
        console.log('connection: ' + socket.connected);
        if (socket.connected) {
            setMessages([]);
            socket.emit('join', {name, room}, (err) => {
                if(err) {
                    alert(err);
                }else {
                    setErr(true)
                }
            });
            // setErr(true);
        }
    }
    let handleInfoChilds;
    let handleMessagesChilds;
    handleInfoChilds = <InfoBar room={room} />;
    if (!err) {
        handleMessagesChilds = <div className="loader" onClick={() => reConnect()}>
            <ClimbingBoxLoader
                css={override}
                color={"#123abc"}
                loading={true}
            />
        </div>;
    } else {
        // handleInfoChilds = <InfoBar room={room} />;
        handleMessagesChilds = <Messages messages={messages} name={name} userColors={userColors}/>;
    }

    return (
        <div className="outerContainer">
            <div className="container" style={{'paddingRight': '0px','paddingLeft': '0px'}}>
                {handleInfoChilds}
                {/* <div className="chat"> */}
                    {/* <div className="chat-container"> */}
                        <div className="conversation">
                            <div className="conversation-container">  
                                {handleMessagesChilds}
                            </div>
                        {/* </div> */}
                        {/* </div> */}
                        <Input message={message} setMessage={setMessage} sendMessage={sendMessage} />
                    </div>
                </div>
                <TextContainer users={users}/>
        </div>
    );
}

export default Chat;
