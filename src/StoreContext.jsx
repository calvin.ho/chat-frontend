import React , { useState } from 'react';
import App from './App';

export const multiContext = React.createContext();

function StoreContext () {
    const [currentName, setCurrentName] = useState('');
    const [currentRoom, setCurrentRoom] = useState('');

    return (
        <multiContext.Provider value={{currentName, setCurrentName, currentRoom, setCurrentRoom}}>
            <App />
        </multiContext.Provider>   
    );
}
export default StoreContext;