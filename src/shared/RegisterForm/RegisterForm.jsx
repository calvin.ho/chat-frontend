import React, { useState } from 'react';
import './RegisterForm.css';
import { Form } from 'react-bootstrap';

const RegisterForm = ({ setRegisterForm }) => {
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const handleSubmit = (e) => {
        e.preventDefault();
        if(name && password) {
            setRegisterForm({
                'username': name,
                'password': password
            });
        };
    }

    return (
        <Form onSubmit={handleSubmit}>
            <Form.Group controlId="username">
                <Form.Label style={{color: "whitesmoke"}}>Username</Form.Label>
                <Form.Control
                    className="JoinInput"  
                    type="text"
                    value={name}
                    onChange={e => setName(e.target.value)}
                 />
            </Form.Group>
            <Form.Group controlId="password">
                <Form.Label style={{color: "whitesmoke"}}>password</Form.Label>
                <Form.Control
                    className="JoinInput"  
                    type="text"
                    value={password}
                    onChange={e => setPassword(e.target.value)} 
                />
            </Form.Group>
            <button className="button mt-20" type="submit" >Register</button>
        </Form> 
    );
}


export default RegisterForm;