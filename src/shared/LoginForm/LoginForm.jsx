import React, { useState, useEffect } from 'react';
import './LoginForm.css';
import { Form } from 'react-bootstrap';
// import _ from 'lodash';
// import { multiContext } from '../../StoreContext';

const LoginForm = ({setLoginForm, options }) => {
    // const { setCurrentName, setCurrentRoom } = useContext(multiContext);
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [room, setRoom] = useState();

    useEffect(() => {
        if(options){
            setRoom(options[0]['id']);
        }
    },[options])

    const eventHandle = (e) => {
        e.preventDefault();
        alert('Please type the username and roomname');
    }

    const saveState = (e) => {
        // setCurrentName(name);
        // setCurrentRoom(room);
    }



    const handleSubmit = (e) => {
        e.preventDefault();
        if(name && password && room) {
            setLoginForm({
                'username': name,
                'password': password,
                'room': room
            });
        };
    };

    return (
        <Form onSubmit={handleSubmit}>
            <Form.Group controlId="username">
                <Form.Label style={{color: "whitesmoke"}}>Username</Form.Label>
                <Form.Control
                    className="JoinInput"  
                    type="text"
                    value={name} 
                    onChange={(event) => setName(event.target.value)} />
            </Form.Group>
            <Form.Group controlId="password">
                <Form.Label style={{color: "whitesmoke"}}>password</Form.Label>
                <Form.Control
                    className="JoinInput"  
                    type="text"
                    value={password} 
                    onChange={(event) => setPassword(event.target.value)} />
            </Form.Group>
            <Form.Group controlId="roomSelector">
                <Form.Label style={{color: "whitesmoke"}}>Room</Form.Label>
                <Form.Control as="select" className="joinSelect" onChange={e => setRoom(e.target.value)}>
                {   
                    options ?
                    options.map((option, index) => {
                        return (<option key={index} value={option.id}>{option.room_name}</option>)
                    }) : ''
                }
                </Form.Control>
            </Form.Group>
            <span onClick={event => (!name || !password || !room) ? eventHandle(event) : saveState(event)} >
                <button className="button mt-20" type="submit" >Sign In</button>
            </span>
        </Form> 
    );
}


export default LoginForm;