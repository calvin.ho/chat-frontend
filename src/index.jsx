import React from 'react';
import ReactDom from 'react-dom';
import App from './App';
import 'emoji-mart/css/emoji-mart.css'
import StoreContext from './StoreContext';

ReactDom.render(<StoreContext><App/></StoreContext>, document.querySelector('#root'));

