FROM node:14.11.0-buster

ARG configuration 

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install
RUN npm install pm2 -g
COPY . .

RUN npm run $configuration
EXPOSE 3000 8000

CMD ["pm2-runtime", "start", "ecosystem.config.js"]
